use simple_dns::dns_packet::DnsPacket;
use std::io::Read;
use std::fs::File;
use std::error::Error;
use simple_dns::packet_buffer::PacketBuffer;

fn main() -> Result<(), Box<dyn Error>> {
    let mut f = File::open("./scratch/response_packet.txt")?;
    let mut buffer = PacketBuffer::new();
    f.read(&mut buffer.buf)?;

    let packet = DnsPacket::from_buffer(&mut buffer)?;
    println!("{:#?}", packet.header);

    for q in packet.questions {
        println!("{:#?}", q);
    }
    for rec in packet.answers {
        println!("{:#?}", rec);
    }
    for rec in packet.authorities {
        println!("{:#?}", rec);
    }
    for rec in packet.resources {
        println!("{:#?}", rec);
    }

    Ok(())
}