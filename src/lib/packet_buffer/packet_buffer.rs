
use crate::packet_buffer::PacketBufferResult;
type Result<T> = PacketBufferResult<T>;

const PACKET_BUFFER_LENGTH: usize = 512;


pub struct PacketBuffer {
    pub buf: [u8; PACKET_BUFFER_LENGTH],
    pub pos: usize,
}

impl PacketBuffer {
    pub fn new() -> Self {
        Self {
            buf: [0; PACKET_BUFFER_LENGTH],
            pos: 0
        }
    }
    pub fn step(&mut self, steps: usize) {
        self.pos += steps;
    }
    pub fn seek(&mut self, pos: usize) {
        self.pos = pos;
    }

    pub fn get(&self, pos: usize) -> Result<u8> {
        if pos > self.buf.len() {
            return Err("End of buffer".into())
        }
        let res = self.buf[pos];
        Ok(res)
    }

    pub fn get_range(&self, start: usize, len: usize) -> Result<&[u8]> {
        if start + len >= 512 {
            return Err("End of buffer".into());
        }
        Ok(&self.buf[start..start + len as usize])
    }

    pub fn read(&mut self) -> Result<u8>{
        let res = self.get(self.pos);
        if let Err(e) = res {
            return Err(e);
        }
        self.step(1);
        res
    }

    pub fn read_u16(&mut self) -> Result<u16> {
        let res = ((self.read()? as u16) << 8) | self.read()? as u16;
        Ok(res) 
    }

    pub fn read_u32(&mut self) -> Result<u32> {
        let mut res = 0u32;
        for i in (0..4).rev() {
            res = res | ((self.read()? as u32) << (i * 8))
        }
        Ok(res)
    }

    pub fn read_qname(&mut self) -> Result<String> {
        let mut pos = self.pos;
        let mut jumped = false;
        let max_jumps = 5;
        let mut jumps_done = 0;
        let mut delim = "";
        let mut res = String::new();

        loop {
            if jumps_done >= max_jumps {
                return Err(format!("Limit of {} jumps exceeded", max_jumps).into());
            }
            let len = self.get(pos)?;

            // Jump if 2 most significant bits are set
            // 0xC0 = 0b11000000
            if len & 0xC0 == 0b11000000 {
                if !jumped {
                    self.seek(pos + 2)
                }
                let b2 = self.get(pos+1)? as u16;
                let offset = (((len as u16) ^ 0xC0) << 8) | b2;
                pos = offset as usize;
                jumped = true;
                jumps_done += 1;
                continue;
            } else {
                pos += 1;
                if len == 0 {
                    break;
                }
                res.push_str(delim);
                let str_buf = self.get_range(pos, len as usize)?;
                res.push_str(&String::from_utf8_lossy(str_buf).to_lowercase());

                delim = ".";
                pos += len as usize;
            }
        }
        if !jumped {
            self.seek(pos);
        }
        Ok(res)
    }

}