use std::str::FromStr;
use std::fmt::Display;
use std::error::Error;



#[derive(Debug)]
pub struct PacketBufferError {
    msg: String,
}
impl Display for PacketBufferError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::result::Result<(), std::fmt::Error> { 
        write!(f, "PacketBufferError: {}", self.msg)
    }
}

impl FromStr for PacketBufferError {
    type Err = Self;
    fn from_str(st: &str) -> std::result::Result<Self, <Self as std::str::FromStr>::Err> {
        let msg = String::from(st);
        Ok(Self {msg})
    }
}

impl From<&str> for PacketBufferError {
    fn from(s: &str) -> Self { 
        Self::from_str(s).unwrap()
    }
}

impl From<String> for PacketBufferError {
    fn from(s: String) -> Self {
        Self {msg: s}
    }
}


impl Error for PacketBufferError {}

pub type PacketBufferResult<T> = Result<T, PacketBufferError>;