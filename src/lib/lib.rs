pub mod packet_buffer;
pub mod dns_header;
pub mod dns_question;
pub mod query_type;
pub mod res_code;
pub mod dns_packet;
pub mod dns_record;

